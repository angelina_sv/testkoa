const Router = require('koa-router');

const productService = require('./product.service');
const categoriesList = require('./categoriesList');

const router = new Router();

router.get('/', async (ctx) => {
    try {
        const list = await productService.getList();
        return ctx.render('productList.pug', { list });
    } catch(err) {
        ctx.throw(err.message);
    }
});

router.post('/', async (ctx) => {
    try {
        await productService.create(ctx.request.body);
        return ctx.redirect('/');
    } catch(err) {
        ctx.throw(err.message);
    }
});

router.post('/:id', async (ctx) => {
    try {
        await productService.update(ctx.params.id, ctx.request.body);
        return ctx.redirect('/');
    } catch(err) {
        ctx.throw(err.message);
    }
});

router.delete('/:id', async (ctx) => {
    try {
        await productService.remove(ctx.params.id);
        return ctx.redirect('/');
    } catch(err) {
        ctx.throw(err.message);
    }
});

router.get('/statistics', async (ctx) => {
    try {
        const list = await productService.getStatistics();
        return ctx.render('productStatistics.pug', { list });
    } catch(err) {
        ctx.throw(err.message);
    }
});

router.get('/create', async (ctx) => {
    return ctx.render('formCreation.pug', { categoriesList });
});

router.get('/update/:id', async (ctx) => {
    const currentData = await productService.getById(ctx.params.id);
    return ctx.render('formUpdate.pug', { id: ctx.params.id, categoriesList, currentData });
});

module.exports = router.routes();
