const Product = require('../entities/Product');

async function getList() {
    const list = await Product.find({});

    if(!list) {
        return 'There are no products';
    } else {
        return list;
    }
}

async function create(data) {
    await Product.create(data);
}

async function update(id, newData) {
    const product = await Product.findById(id);

    product.name = newData.name;
    product.category = newData.category;
    product.price = newData.price;
    product.quantityInStock = newData.quantityInStock;

    await product.save();
}

async function remove(id) {
    await Product.findByIdAndDelete(id);
}

async function getStatistics() {
    const result = await Product.aggregate([ {
        $group: {
            _id: '$category',
            'avgProductPrice': {
                $avg: '$price'
            },
            'numOfProducts': {
                $sum: 1
            },

            'totalProductsPrice': {
                $accumulator: {
                    init: function() {
                        return { sum: 0 }
                      },
                    accumulate: function(state, price, quantityInStock) {
                      return {
                        sum: state.sum + (price * quantityInStock)
                      }
                    },
                    accumulateArgs: ['$price', '$quantityInStock'],
                    merge: function(state1, state2) {
                        return {
                          sum: state1.sum + state2.sum
                        }
                      }
                }
            },

            'minPrice': {
                $min: '$price'
            },
            'maxPrice': {
                $max: '$price'
            }
        }
    } ]);

    return result;
}

async function getById(id) {
    const product = await Product.findById(id);
    return product;
}

module.exports = {
    getList,
    create,
    update,
    remove,
    getStatistics,
    getById,
};
