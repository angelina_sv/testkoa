async function update(id) {
    const response = await fetch(`/update/${id}`, {
        method: 'GET'
    });

    window.location.href = response.url;
}

async function remove(id) {
    const response = await fetch(`/${id}`, {
        method: 'DELETE'
    });

    if (response.redirected) {
        window.location.href = response.url;
    }
}
