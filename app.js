const dotenv = require("dotenv");

dotenv.config();

const Koa = require('koa');
const path = require('path');
const views = require('koa-views');

const config = require('./config');
const bodyParser=  require('./handlers/bodyParser');
const errors = require('./handlers/errors');
const mongoose = require('./mongoose.config');
const product = require('./product/product.controller');

const app = new Koa();

const render = views(
    path.join(process.cwd(), 'views'), 
    { 
        extension: 'pug' 
    }
);
  
app.use(render);
app.use(bodyParser);
app.use(errors);

app.use(product);
mongoose();

module.exports = app;
