module.exports = {
    port: process.env.PORT || 3000,
    mongoDb: process.env.MONGO_DB,
};
