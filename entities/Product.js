// const { Shema, model } = require('mongoose');
const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const schema = new Shema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    category: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quantityInStock: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Product', schema);
