const mongoose = require('mongoose');

const config = require('./config');

module.exports = async () => {
    try {
        await mongoose.connect(
            config.mongoDb, 
            { 
                useNewUrlParser: true, 
                useUnifiedTopology: true, 
                useCreateIndex: true 
            }
        );
    } catch(err) {
        console.log(err);
    }
};
